import java.util.Scanner;
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner teclado=new Scanner(System.in);
		int edad, n, edadMaxima=0, edadMinima=0, sumaMayores=0, sumaMenores=0;
		double sumaTotales=0, mediaEdades;
		String nombre;
		System.out.println("�Cuantos alumnos hay en la clase?");
		n=teclado.nextInt();
		teclado.nextLine();
		for(int i=1;i<=n;i++){
			System.out.println("Introduce nombre del alumno: ");
			nombre=teclado.nextLine();
			System.out.println("Introduce edad del alumno: ");
			edad=teclado.nextInt();
			teclado.nextLine();
			
			if(edad<18){
				System.out.println(nombre+" es menor de edad");
				if(edad>=16){
					System.out.println("Matricula gratis");
				}
				else{
					System.out.println("No puedes estar matriculado por edad");
				}
				sumaMenores=sumaMenores+edad;
			}
			else{
				System.out.println(nombre+" es mayor de edad");
				if(edad%2==0){
					System.out.println("Bonificaci�n en la matr�cula del 20%");
				}
				else{
					System.out.println("Deber� pagar la matr�cula sin bonificaci�n");
				}
				sumaMayores=sumaMayores+edad;
			}
			if(i==1){
				edadMaxima=edad;
				edadMinima=edad;
			}
			if(edad>edadMaxima){
				edadMaxima=edad;
			}
			if(edad<edadMinima){
				edadMinima=edad;
			}
			sumaTotales=sumaTotales+edad;
						
		}
		mediaEdades=sumaTotales/n;
		System.out.println("La edad maxima es: "+edadMaxima);
		System.out.println("La edad minima es: "+edadMinima);
		System.out.println("La suma de los mayores de edad es: "+sumaMayores);
		System.out.println("La suma de los menores de edad es: "+sumaMenores);
		System.out.println("La media de edades es: "+mediaEdades);
	}

}
