import java.util.Scanner;
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner teclado=new Scanner(System.in);
		int n, resultado, resultadoCorrecto, contadorCorrectos=0, contadorErrores=0; 
		double mediaNumeros, contadorNumeros=0, sumaNumeros=0;
		do{
			do{
				System.out.print("Pulse un valor (0=salir): ");
				n=teclado.nextInt();
				if(n<0){
					System.out.println("Error, el numero ha de ser positivo");
				}
			}
			while(n<0);
			if(n%2==0 && n!=0){
				System.out.println("\tCOMPROBAR LA TABLA DEL "+n);
				for(int i=1; i<=10; i++){
					System.out.print("\t"+n+"*"+i+"=");
					resultado=teclado.nextInt();
					resultadoCorrecto=n*i;
					if(resultado==resultadoCorrecto){
						System.out.println("\tBien");
						contadorCorrectos++;
					}
					else{
						System.out.println("\tError. Es "+resultadoCorrecto);
						contadorErrores++;
					}
				}
				System.out.println("\tAciertos "+contadorCorrectos);
				System.out.println("\tErrores "+contadorErrores);
			}
			if(n%2!=0){
				System.out.println("\tTABLA DEL "+n+" EN ORDEN DECR.");
				for(int i=10;i>=1;i--){
					resultado=n*i;
					System.out.println("\t"+n+"*"+i+"="+resultado);					
				}
			}
			if(n>0){
				sumaNumeros=sumaNumeros+n;
				contadorNumeros++;
			}
			
		}
		while(n!=0);
		mediaNumeros=sumaNumeros/contadorNumeros;
		System.out.println("La media de todos los numeros correctos es "+mediaNumeros);
	}

}
